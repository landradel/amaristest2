﻿using RIM.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RIM.Entities.Concrete;
using RIM.Utilities;

namespace RIM.Data
{
    public class RoverData : IRover
    {
        /// <summary>
        /// Get the Rover Result entity
        /// </summary>
        /// <param name="Rover">Rover entity to process</param>
        /// <returns></returns>
        public RoverResultEntity ProcessRover(RoverEntity Rover)
        {
            // Set command orders
            RoverResultEntity resultEntity = GetRoverCommands(Rover);

            // Get string from result type enum
            resultEntity.ResultTypeString = resultEntity.ResultType.GetStringValue();

            // Get string from actual direction enum
            resultEntity.ActualDirectionString = resultEntity.ActualDirection.GetStringValue();

            return resultEntity;
        }

        /// <summary>
        /// Process command string and set dictionary of command orders
        /// </summary>
        /// <param name="Command"></param>
        private RoverResultEntity GetRoverCommands(RoverEntity Rover)
        {
            // Create the rover result entity
            RoverResultEntity roverResultEntity = new RoverResultEntity(true);

            // Validate if Command object is not null
            if (Rover != null)
            {
                // Validate if Command isn't null or empty or whitespace
                if (!string.IsNullOrEmpty(Rover.Command) && !string.IsNullOrWhiteSpace(Rover.Command))
                {
                    /* Command process logic */

                    // Get string length
                    int commandLength = Rover.Command.Length;

                    // Validate if command length is differente than 0
                    if (commandLength != 0)
                    {
                        // Low and trim command
                        string trimmedAndLowedCommand = Rover.Command.ToLower().Trim();

                        // Iterate throught string
                        for (int index = 0; index < commandLength; index++)
                        {
                            // Check if the command is invalid
                            if (!roverResultEntity.Result)
                            {
                                // break for iteration
                                break;
                            }

                            // Get char from string command
                            string commandCharString = trimmedAndLowedCommand.Substring(index, 1);

                            // Valdate command char value
                            if (!string.IsNullOrEmpty(commandCharString))
                            {
                                // Check if the string contains any of the letters ALR
                                if ("alr".Contains(commandCharString.ToString()))
                                {
                                    // Set Command order char and index value
                                    roverResultEntity.SetCommand(index, commandCharString);
                                }
                                else
                                {
                                    // Set invalid result
                                    roverResultEntity.Result = false;

                                    // Set invalid command type
                                    roverResultEntity.ResultType = ResultTypeEnum.InvalidCommand;
                                }
                            }
                        }

                        // Check for result after iteration
                        if (roverResultEntity.Result)
                        {
                            // Set coordinates recieved by rover entity
                            roverResultEntity.XCoordinate = Rover.XCoordinate;
                            roverResultEntity.YCoordinate = Rover.YCoordinate;

                            // Get actual coordinates
                            int actualXCoordinate = roverResultEntity.XCoordinate;
                            int actualYCoordinate = roverResultEntity.YCoordinate;

                            roverResultEntity.ActualDirection = Rover.ActualDirection;
                            roverResultEntity.ActualDirectionString = Rover.ActualDirectionString;
                            // Process command
                            ProcessCommands(ref roverResultEntity);

                            // Validate if the new coordinate is a valid coordinate
                            if (roverResultEntity.XCoordinate < 0 || 
                                roverResultEntity.XCoordinate > Rover.StageWidth ||
                                roverResultEntity.YCoordinate > Rover.StageHeight ||
                                roverResultEntity.YCoordinate < 0)
                            {
                                // Set invalid result
                                roverResultEntity.Result = false;

                                // Set invalid command type
                                roverResultEntity.ResultType = ResultTypeEnum.CommandOutOfStage;
                            }

                        }
                    }
                }
                else
                {
                    // Set invalid result
                    roverResultEntity.Result = false;

                    // Set invalid command type
                    roverResultEntity.ResultType = ResultTypeEnum.InvalidCommand;
                }
            }

            return roverResultEntity;
        }

        /// <summary>
        /// Iterates throught all the commands and validate the logic of the command itself
        /// </summary>
        /// <param name="roverResultEntity"></param>
        private void ProcessCommands(ref RoverResultEntity roverResultEntity)
        {
            // Create instance of coordinates
            roverResultEntity.Coordinates = new List<SpecialCoordinate>();

            int commandLength = roverResultEntity.GetCommandLength();

            // Iterate commands
            for (int i = 0; i < commandLength; i++)
            {
                string commandValue = roverResultEntity.GetCommandByIndex(i);

                switch (commandValue)
                {
                    case "a":
                        // Advance
                        Advance(ref roverResultEntity);

                        // Add new coordinate
                        roverResultEntity.Coordinates.Add(new SpecialCoordinate(i, roverResultEntity.XCoordinate, roverResultEntity.YCoordinate));

                        break;
                    case "l":
                        // Change actual direction
                        roverResultEntity.ActualDirection = ChangeActualDirection(roverResultEntity.ActualDirection, commandValue);
                        break;
                    case "r":
                        // Change actual direction
                        roverResultEntity.ActualDirection = ChangeActualDirection(roverResultEntity.ActualDirection, commandValue);
                        break;
                    default:
                        break;
                }
            }

        }

        private void Advance(ref RoverResultEntity roverResultEntity)
        {
            switch (roverResultEntity.ActualDirection)
            {
                case DirectionEnum.North:
                    roverResultEntity.YCoordinate = roverResultEntity.YCoordinate - 20;
                    break;
                case DirectionEnum.South:
                    roverResultEntity.YCoordinate = roverResultEntity.YCoordinate + 20;
                    break;
                case DirectionEnum.East:
                    roverResultEntity.XCoordinate = roverResultEntity.XCoordinate + 20;
                    break;
                case DirectionEnum.West:
                    roverResultEntity.XCoordinate = roverResultEntity.XCoordinate - 20;
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Logic of directions
        /// </summary>
        /// <param name="ActualDirection">Get the enum of the actual direction</param>
        /// <param name="commandValue">Get the command: it must be "l" or "r"</param>
        /// <returns>Returns the new direction</returns>
        private DirectionEnum ChangeActualDirection(DirectionEnum ActualDirection, string commandValue)
        {
            DirectionEnum newDirection = DirectionEnum.North;

            switch (ActualDirection)
            {
                case DirectionEnum.North:
                    if (commandValue == "l")
                    {
                        newDirection = DirectionEnum.West;
                    }
                    else
                    {
                        newDirection = DirectionEnum.East;
                    }
                    break;
                case DirectionEnum.South:
                    if (commandValue == "l")
                    {
                        newDirection = DirectionEnum.East;
                    }
                    else
                    {
                        newDirection = DirectionEnum.West;
                    }
                    break;
                case DirectionEnum.East:
                    if (commandValue == "l")
                    {
                        newDirection = DirectionEnum.North;
                    }
                    else
                    {
                        newDirection = DirectionEnum.South;
                    }
                    break;
                case DirectionEnum.West:
                    if (commandValue == "l")
                    {
                        newDirection = DirectionEnum.South;
                    }
                    else
                    {
                        newDirection = DirectionEnum.North;
                    }
                    break;
                default:
                    break;
            }

            return newDirection;
        }
    }
}
